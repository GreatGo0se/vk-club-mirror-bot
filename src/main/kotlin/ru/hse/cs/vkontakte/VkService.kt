package ru.hse.cs.vkontakte

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import name.anton3.vkapi.client.VkClient
import name.anton3.vkapi.client.VkClientFactory
import name.anton3.vkapi.method.ServiceMethod
import name.anton3.vkapi.tokens.ServiceToken
import name.anton3.vkapi.transport.apacheClientFactory
import name.anton3.vkapi.transport.defaultHttpAsyncClient
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient
import ru.hse.cs.constants.VK_CLIENT_SECRET
import ru.hse.cs.constants.VK_TOKEN

class VkService {
    val apacheClient: CloseableHttpAsyncClient = defaultHttpAsyncClient()
    val clientFactory: VkClientFactory = apacheClientFactory(apacheClient)
    val vkClient: VkClient<ServiceMethod> = clientFactory.secure(ServiceToken(VK_TOKEN, VK_CLIENT_SECRET))

    fun stop() {
        GlobalScope.launch {
            clientFactory.closeAndJoin()
        }
        apacheClient.close()
    }
}