package ru.hse.cs.model

data class Group(
    val id: Int,
    val name: String
)
