package ru.hse.cs.model

data class Post(
    val id: Int,
    val group: Group,
    val text: String,
    val attachments: List<String>
)