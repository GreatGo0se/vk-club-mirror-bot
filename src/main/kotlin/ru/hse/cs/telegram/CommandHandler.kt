package ru.hse.cs.telegram

import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ticker
import me.ivmg.telegram.Bot
import me.ivmg.telegram.entities.ParseMode
import me.ivmg.telegram.entities.Update
import name.anton3.vkapi.client.VkClient
import name.anton3.vkapi.generated.groups.methods.GroupsGetById
import name.anton3.vkapi.generated.photos.objects.Photo
import name.anton3.vkapi.generated.wall.methods.WallGet
import name.anton3.vkapi.generated.wall.objects.GetFilter
import name.anton3.vkapi.generated.wall.objects.WallpostFull
import name.anton3.vkapi.method.ServiceMethod
import ru.hse.cs.constants.CIRCULAR_ARRAY_SIZE
import ru.hse.cs.constants.UPDATE_DELAY_IN_SECONDS
import ru.hse.cs.constants.VK_LINK
import ru.hse.cs.extensions.*
import ru.hse.cs.model.Group
import ru.hse.cs.model.Post
import ru.hse.cs.utils.CircularArray
import ru.hse.cs.vkApi
import java.util.*
import java.util.concurrent.ConcurrentHashMap

@ObsoleteCoroutinesApi
@ExperimentalCoroutinesApi
class TgHandler(val vkClient: VkClient<ServiceMethod>, val bot: Bot) {
    private val chatIds: MutableSet<Long> = ConcurrentHashMap.newKeySet()
    private val chatToGroups = ConcurrentHashMap<Long, MutableSet<Group>>()
    private val chatToChannel = ConcurrentHashMap<Long, Channel<Post>>()
    private val chatToSentMessageCache = ConcurrentHashMap<Long, CircularArray<Pair<Int, Int>>>()
    private val chatToWorker = ConcurrentHashMap<Long, Job>()

    init {
        val tickerChannel = ticker(UPDATE_DELAY_IN_SECONDS)

        GlobalScope.launch {
            while (!tickerChannel.isClosedForReceive) {
                tickerChannel.receive()
                val now = Date()

                chatIds.forEach { chatId ->
                    val groups = chatToGroups[chatId]!!

                    groups.forEach { g ->
                        GlobalScope.launch {
                            val req = WallGet(
                                ownerId = -g.id,
                                count = 2,
                                filter = GetFilter.OWNER
                            )
                            val resp = vkApi.vkClient(req)
                            resp.filter { post ->
                                    post.date!!.after(Date(now.time - UPDATE_DELAY_IN_SECONDS - 5000))
                                }
                                .forEach { addPostToQueue(chatId, parsePost(it, g)) }
                        }
                    }
                }
            }
        }
    }

    fun handleReceiveCommand(update: Update, args: List<String>) {
        val chatId = update.message!!.chat.id

        val num = args.firstOrNull()?.toIntOrNull() ?: 5

        val groups = chatToGroups.getOrDefault(chatId, mutableSetOf())

        groups.forEach { group ->
            val m = WallGet(
                ownerId = -group.id,
                count = if (num > 5) 5 else num,
                filter = GetFilter.OWNER
            )

            GlobalScope.launch {
                val r = vkClient.invoke(m)
                r.forEach { wallPost ->
                    if (wallPost.containsAds()) return@forEach

                    val post = parsePost(wallPost, group)
                    GlobalScope.launch {
                        addPostToQueue(chatId, post)
                    }
                }
            }
        }
    }


    fun handleAllCommand(update: Update) {
        val groups = chatToGroups.getOrPut(update.message!!.chat.id) { mutableSetOf() }.mapIndexed { i, g -> i to g }

        val text = when {
            groups.isEmpty() -> "You have not subscribed yet"
            else -> "Your subscriptions are: ${groups.joinToString(
                "\n",
                "\n"
            ) { "${it.first + 1}. ${it.second.getMarkdownLink()} _${it.second.id}_" }}"
        }

        bot.sendMessage(
            chatId = update.message!!.chat.id,
            text = text,
            parseMode = ParseMode.MARKDOWN,
            disableWebPagePreview = true
        )
    }

    @ExperimentalCoroutinesApi
    @ObsoleteCoroutinesApi
    fun handleAddCommand(
        update: Update,
        args: List<String>
    ) {
        val chatId = update.message!!.chat.id

        if (!chatIds.contains(chatId)) {
            initChat(chatId)
        }

        val groups = chatToGroups.getOrPut(chatId) { mutableSetOf() }

        val newGroups = args
            .map(String::trim)
            .map(String::getPublicId)

        val getGroupResponse = GroupsGetById(newGroups)

        GlobalScope.launch {
            val foundGroups = vkClient(getGroupResponse).map { Group(it.id!!, it.name!!) }
            val groupList = foundGroups.joinToString("\n", "\n") { "${it.getMarkdownLink()} _${it.id}_" }

            bot.sendMessage(
                chatId,
                "Successfully subscribed to: $groupList",
                parseMode = ParseMode.MARKDOWN,
                disableWebPagePreview = true
            )

            groups.addAll(foundGroups)
        }
    }

    fun handleRemoveCommand(update: Update, args: List<String>) {
        val chatId = update.message!!.chat.id
        val groups = chatToGroups.getOrDefault(chatId, mutableSetOf())
        val toDelete = args.mapNotNull { arg -> arg.toIntOrNull() }
        val removed = mutableListOf<Group>()
        groups.removeAll { g ->
            toDelete.contains(g.id).also { v ->
                if (v) removed.add(g)
            }
        }

        val message = if (removed.isEmpty()) "Nothing removed" else "Removed:\n${removed.joinToString(
            "\n"
        ) { "${it.getMarkdownLink()} _${it.id}_" }}"

        bot.sendMessage(
            chatId = chatId,
            text = message,
            parseMode = ParseMode.MARKDOWN,
            disableWebPagePreview = true
        )
    }

    fun handleRemoveAllCommand(update: Update) {
        val chatId = update.message!!.chat.id
        chatToGroups[chatId]?.clear()
        bot.sendMessage(chatId, "All subscriptions removed")
    }


    fun addGroupsToChat(chatId: Long, groups: Set<Group>) {
        if (!chatIds.contains(chatId)) {
            initChat(chatId)
        }

        chatToGroups[chatId]!!.addAll(groups)
    }

    fun getChatWithGroups(): Map<Long, Set<Group>> {
        return chatToGroups.toList().mapNotNull { (chatId, groups) ->
            when {
                groups.isEmpty() -> null
                else -> chatId to groups
            }
        }.toMap()
    }

    private fun initChat(chatId: Long) {
        chatIds.add(chatId)
        chatToGroups[chatId] = mutableSetOf()
        chatToChannel[chatId] = Channel()
        chatToSentMessageCache[chatId] = CircularArray(CIRCULAR_ARRAY_SIZE)

        val worker = GlobalScope.launch {
            val postChannel = chatToChannel[chatId]

            while (!postChannel!!.isClosedForReceive) {
                val post = postChannel.receive()

                chatToSentMessageCache[chatId]!!.add(post.group.id to post.id)

                when {
                    post.attachments.isEmpty() -> bot.sendMessage(chatId, post.text, disableWebPagePreview = true)
                    else -> bot.sendPage(chatId, post.text, post.attachments)
                }
            }
        }

        chatToWorker[chatId] = worker
    }

    private fun parsePost(
        wallPost: WallpostFull,
        group: Group
    ): Post {
        val attachments = (wallPost.attachments ?: emptyList())
            .filter { att -> att.type == "photo" }
            .mapNotNull { att -> (att.body as Photo).sizes!!.last().url }

        val header = "[${group.name}]"
        val content = with(wallPost.text ?: "") {
            when {
                attachments.isNotEmpty() -> truncate(900)
                else -> truncate(1000)
            }
        }

        val link = "$VK_LINK/wall-${group.id}_${wallPost.id!!}"
        val text = listOf(header, content, link).filter(String::isNotBlank).joinToString("\n\n")

        return Post(
            id = wallPost.id!!,
            group = group,
            text = text,
            attachments = attachments
        )
    }

    private suspend fun addPostToQueue(chatId: Long, post: Post) {
        val messageCache = chatToSentMessageCache[chatId]!!

        if (messageCache.contains(post.group.id to post.id)) {
            return
        }

        chatToChannel[chatId]!!.send(post)
    }
}
