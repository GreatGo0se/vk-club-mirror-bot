package ru.hse.cs

import me.ivmg.telegram.Bot
import me.ivmg.telegram.bot
import me.ivmg.telegram.dispatch
import me.ivmg.telegram.dispatcher.command
import me.ivmg.telegram.dispatcher.telegramError
import me.ivmg.telegram.dispatcher.text
import okhttp3.logging.HttpLoggingInterceptor
import org.json.JSONObject
import ru.hse.cs.constants.*
import ru.hse.cs.extensions.putAll
import ru.hse.cs.model.Group
import ru.hse.cs.telegram.TgHandler
import ru.hse.cs.vkontakte.VkService
import java.io.File
import java.util.*
import kotlin.concurrent.timerTask
import kotlin.system.exitProcess

lateinit var savedChats: File
lateinit var bot: Bot
lateinit var handler: TgHandler
val vkApi = VkService()

fun main() {
    initTg()
    loadData()
    bot.startPolling()
}

private fun loadData() {
    savedChats = File("bot-chats").also { dir -> dir.mkdir() }
    val loaded = savedChats.listFiles()
        .mapNotNull { f ->
            if (f.isDirectory || f.name.toLongOrNull() == null) {
                return@mapNotNull null
            }

            val chatId = f.name.toLong()
            val groups = f.readLines().map { line ->
                with(JSONObject(line)) {
                    val id = get("id")!!.toString().toInt()
                    val name = get("name")!!.toString()
                    Group(id, name)
                }
            }.toMutableSet()
            chatId to groups
        }

    loaded.forEach { (chatId, groups) -> handler.addGroupsToChat(chatId, groups) }
}


private fun saveData() {
    val data = handler.getChatWithGroups()
    savedChats.listFiles().forEach { f -> f.deleteRecursively() }
    data.forEach { chat ->
        val chatId = chat.key
        val groups = chat.value
        val file = savedChats.toPath().resolve(chatId.toString()).toFile().also { it.createNewFile() }

        val preparedData = groups.map { g ->
            JSONObject().putAll("id" to g.id.toString(), "name" to g.name)
        }.joinToString("\n")

        file.writeText(preparedData)
    }
}

private fun initTg() {
    bot = bot {
        token = TG_TOKEN
        timeout = 30
        logLevel = HttpLoggingInterceptor.Level.BODY

        dispatch {
            command("start") { bot, update ->
                bot.sendMessage(chatId = update.message!!.chat.id, text = "Ready to be useful")
            }

            command(ADD_COMMAND) { _, update, args ->
                when {
                    args.isEmpty() -> bot.sendMessage(update.message!!.chat.id, "Public ids were expected")
                    else -> handler.handleAddCommand(update, args)
                }
            }

            command(ALL_COMMAND) { bot, update ->
                handler.handleAllCommand(update)
            }

            command(RECEIVE_COMMAND) { bot, update, args ->
                handler.handleReceiveCommand(update, args)
            }

            command(REMOVE_COMMAND) { bot, update, args ->
                when {
                    args.isEmpty() -> bot.sendMessage(update.message!!.chat.id, "Public ids were expected")
                    else -> handler.handleRemoveCommand(update, args)
                }
            }

            command(REMOVE_ALL_COMMAND) { bot, update ->
                handler.handleRemoveAllCommand(update)
            }

            text(SHUTDOWN_COMMAND) { bot, update ->
                val chatId = update.message!!.chat.id
                bot.sendMessage(chatId, "Bot is shutdown")
                stopSystem(bot)
            }

            telegramError { _, telegramError ->
                println(telegramError.getErrorMessage())
            }
        }
    }

    handler = TgHandler(vkApi.vkClient, bot)
}

private fun stopSystem(bot: Bot) {
    bot.stopPolling()
    vkApi.stop()
    saveData()

    Timer().schedule(timerTask {
        exitProcess(0)
    }, 1000 * 5)
}
