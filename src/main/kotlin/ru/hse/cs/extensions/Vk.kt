package ru.hse.cs.extensions

import name.anton3.vkapi.generated.wall.objects.WallpostFull

fun WallpostFull.containsAds() = markedAsAds?.value == true || text?.toLowerCase()?.contains("смотри источник") == true

fun String.getPublicId() = when {
    startsWith("public") -> substring(6)
    else -> this
}