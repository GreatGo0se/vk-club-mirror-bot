package ru.hse.cs.extensions

fun String.truncate(size: Int, truncateOffset: Int = 50) =
    if (this.length > size) "${this.take(size - truncateOffset)}..." else this