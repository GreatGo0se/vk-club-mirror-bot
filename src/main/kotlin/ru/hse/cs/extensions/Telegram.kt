package ru.hse.cs.extensions

import me.ivmg.telegram.Bot
import org.json.JSONObject
import ru.hse.cs.constants.TG_API_LINK
import ru.hse.cs.constants.TG_TOKEN
import ru.hse.cs.constants.VK_LINK
import ru.hse.cs.model.Group

fun Bot.sendPage(chatId: Long, text: String, photoUrls: List<String>) {
    val firstMedia = JSONObject()
        .putAll(
            "caption" to text,
            "type" to "photo",
            "media" to photoUrls.first()
        )

    val restMedia =
        photoUrls.drop(1).map {
            JSONObject()
                .putAll(
                    "type" to "photo",
                    "media" to it
                )
        }

    val media = listOf(firstMedia) + restMedia

    val body = JSONObject().put("chat_id", chatId).put("media", media)

    khttp.post(
        url = "$TG_API_LINK/bot$TG_TOKEN/sendMediaGroup",
        headers = mapOf("Content-Type" to "application/json"),
        data = body.toString()
    )
}