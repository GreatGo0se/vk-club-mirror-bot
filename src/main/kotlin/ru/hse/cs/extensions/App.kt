package ru.hse.cs.extensions

import ru.hse.cs.constants.VK_LINK
import ru.hse.cs.model.Group

fun Group.getMarkdownLink() = "[$name]($VK_LINK/public$id)"
