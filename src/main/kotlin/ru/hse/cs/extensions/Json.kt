package ru.hse.cs.extensions

import org.json.JSONObject

fun JSONObject.putAll(vararg pairs: Pair<String, String>): JSONObject {
    pairs.forEach { (k, v) -> put(k, v) }
    return this
}
