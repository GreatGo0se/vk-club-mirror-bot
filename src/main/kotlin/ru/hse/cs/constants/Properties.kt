package ru.hse.cs.constants

val UPDATE_DELAY_IN_SECONDS = 1000L * System.getenv("UPDATE_DELAY_IN_SECONDS")!!.toLong()
const val CIRCULAR_ARRAY_SIZE = 32
