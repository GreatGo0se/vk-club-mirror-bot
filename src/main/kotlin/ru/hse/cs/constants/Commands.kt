package ru.hse.cs.constants

const val ADD_COMMAND = "add"
const val ALL_COMMAND = "all"
const val RECEIVE_COMMAND = "receive"
const val REMOVE_COMMAND = "remove"
const val REMOVE_ALL_COMMAND = "remove_all"
val SHUTDOWN_COMMAND = System.getenv("SHUTDOWN_COMMAND")!!