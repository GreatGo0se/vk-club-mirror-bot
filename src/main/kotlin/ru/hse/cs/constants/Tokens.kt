package ru.hse.cs.constants

val TG_TOKEN = System.getenv("TG_TOKEN")!!
val VK_TOKEN = System.getenv("VK_TOKEN")!!
val VK_CLIENT_SECRET = System.getenv("VK_SECRET")!!